<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
'Edit the first three lines below. 
'They contain the email address to send the form to, the from address and the email subject.
'Use Your Palomar email address for the "from" address and the "to" address. Preserve the quotation marks.
varTo = "klisenbe6194@student.palomar.edu"
varFrom = "klisenbe6194@student.palomar.edu"
varSubject = "Web Site Form Response"

'Server and Server Port Information:
'This is set correcttly for the GC server - don't change it for the GC Server
'For a different server, you might have to change the values.
'You may have to get this info from the server administrator.
varServer = "smtp-server.palomar.edu"
varPort = 25

'no more editing required

If Len(Request.Form) > 0 Then 'send email

varMessage = "The following information was submitted on " & Now() & ":" & vbNewLine & vbNewLine 
For i = 1 To Request.Form.Count 
  FieldName = Request.Form.Key(i) 
  FieldValue = Request.Form.Item(i) 
  varMessage = varMessage & FieldName & ": " & FieldValue & vbNewLine 
Next 

Set Mailer = CreateObject("CDO.Message")
Mailer.To = varTo
Mailer.From = varFrom
Mailer.Subject = varSubject
Mailer.TextBody = varMessage
Mailer.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
' Server Name
Mailer.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = varServer
' Server Port
Mailer.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = varPort
Mailer.Configuration.Fields.Update
Mailer.Send()
Set Mailer = Nothing

End If 'End check if form was submitted %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Thank You for Submitting the Form</title>
<style type="text/css">
<!--
body {
	font: .8em/1.5em Verdana, Geneva, Arial, Helvetica, sans-serif;
	color: #333;
	margin: 50px;
}
h1 {
	font-family: "Trebuchet MS", Verdana, Geneva, Arial, Helvetica, sans-serif;
	font-weight: normal;
	text-align: center;
	font-style: italic;
	line-height: 1.2em;
	margin-bottom:0;
}
#FormInfo {
	margin: 0 auto 0 auto;
	width: 35em;
	text-align: left;
}
p {
	margin: 40px 0 3px 0;
}
p.FieldList {
	margin: 0px 0px 0px 5em;
	text-indent:-2em
}

-->
</style>
</head>
<body>
<h1>Thank You for Submitting the Form</h1>
<div id="FormInfo">
  <p>This is the information that you submitted:</p>
  <%
varDisplay = ""  
If Len(Request.Form) > 0 Then 'send email
	For i = 1 To Request.Form.Count 
  	FieldName = Request.Form.Key(i) 
  	FieldValue = Request.Form.Item(i) 
  	varDisplay = varDisplay & "<p class='FieldList'><strong>" & FieldName & ":</strong> " & FieldValue & "</p>" & vbNewLine 
Next

Response.Write varDisplay
End If 
%>

<p class="emailAddress">This email is being sent to: <%= varTo %></p>

</div>
</body>
</html>
