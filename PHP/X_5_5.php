<html>
    <body>
        <h1>5.5 </h1>
<?php
    //************************ 
    // Gather data from form
    //************************
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $pet1 = $_POST['pet1'];
    $pet2 = $_POST['pet2'];
    $pet3 = $_POST['pet3'];
            
    $filename = 'data/'.'X_5_5.txt';
        
    //************************ 
    // Add to file
    //************************   
    $fp = fopen($filename, 'a');
    $output_line =  $lastname.'|'.$firstname.'|'.$pet1.'|'."\n";
    
        fwrite($fp, $output_line);
        fclose($fp);
        
        print "<h2> $lastname, $firstname Added to File </h2>";
    //**************************** 
    // Read from file into table
    //****************************          
    ?>    
      
    <table border ='1'>
       <tr>
        <th>Last Name</th>
        <th>First Name</th>
        <th>Pet 1 </th>
        <th>Pet 2 </th>
        <th>Pet 3 </th>
        </tr> 
    <?php
        $display = "";
        $line_ctr = 0;
    
    $fp = fopen($filename, 'r');
        while (true)
        {
            $line = fgets($fp);
            if (feof($fp))
            {
                break;
            }
            $line_ctr++;
            
            $line_ctr_remainder  = $line_ctr % 2;
            
            if ($line_ctr_remainder === 0)
            {
                $style = "style='background-color: #ffffcc;'";
            } else {
                $style = "style='background-color: #white;'";
            }
            
            list($lastname, $firstname) = explode('|', $line);
            
            $display .="<tr $style>";
            $display .= "<td>".$lastname."</td>";
            $display .= "<td>".$firstname."</td>";
            $display .="<td>".$pet1."</td>";
            $display .="<td>".$pet2."</td>";
            $display .="<td>".$pet3."</td>";
            $display .= "</tr>\n";
        }
        fclose($fp);
        
        print $display;
        
        ?>
        </table> 
    </body>
</html>