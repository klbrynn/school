<html>
<body>

 <?php
    
    $dog_names = "Kastiel, Luna, Sunshine, Wookiee";
    
    $search_term = $_POST['search'];
    
    $pos = stripos($dog_names, $search_term); 
    
    if ($pos === false)
    {
        print "<br>$search_term not found in $dog_names";
    } else {
        print "<br>$search_term is in $dog_names";
    }
?>
    

</body>
</html>