<html>
<body>
    
<?php
    
    $citysearch = $_POST['citysearch'];
    
    $filename = 'data/'.'cities.txt';
    
    if (!file_exists($filename))
    {
        print "Cities file is missing";
        exit;
    }
    
    $lines_in_file = count(file($filename));
    
    $fp = fopen($filename, 'r');
    
    for ($ii = 1; $ii <= $lines_in_file; $ii++)
    {
        $line = fgets($fp);
        $city = trim($line);
        
        $pos = stripos($city, $citysearch);
        
        if ($pos !== false)
        {
            print "<p>The text in" .$citysearch. "was contained in" .$city. "</p>";
        }
    }
    fclose($fp);
?>

</body>
</html>