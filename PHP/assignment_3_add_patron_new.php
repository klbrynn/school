<!DOCTYPE HTML>

<html>
<head>
	<title>Assignment 3 - Add Patron</title>
	<link rel="stylesheet" type="text/css" href="css/kinglib_3.css" >
</head>

<body>

<div id="logo">
	<img src="images/KingLibLogo.jpg" alt="King Real Estate Logo">
</div>

<div id="patron">

<?php
//***************************************
// Gather Data from Form
//***************************************


$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$email = $_POST['email'];
$birth_yyyy = $_POST['birth_yyyy'];
$city = $_POST['city'];


//***************************************
//Validate Data
//***************************************

$error_count = 0;

if (empty($firstname))
{
	print "<br >Error:  You must enter a First Name";
	$error_count++;
}

if (empty($lastname))
{
	print "<br >Error:  You must enter a Last Name";
	$error_count++;
}

if (empty($email))
{
	print "<br >Error:  You must enter your Email";
	$error_count++;
}

if (empty($birth_yyyy))
{
	print "<br >Error:  You must enter your Birth Year";
	$error_count++;
} else {
	if (!is_numeric($birth_yyyy))
	{
		print "<br >Error:  Birth Year must be numeric";
		$error_count++;
	}
}

if ($city == '-')
{
	print "<br >Error:  You must select a City";
	$error_count++;
}

if ($error_count > 0)
{
	print "<br ><br >Go BACK and make corrections";
	print "</body></html>";
	exit;
}



//***************************************
//Calculate Age to Determine Section
//***************************************

$current_year = date('Y');

$patron_age = $current_year - $birth_yyyy;

if ($patron_age > 55)
{
	$section = "Senior";
} elseif ($patron_age > 16)
{
    $section = "Adult";
} else {
	$section = "Children";
}

//***************************************
//Display New Page
//***************************************

$fullname = "$firstname $lastname";

print "<p class='topofdiv'>Thank You for Registering!</p>";

print "<p>Name: $fullname </p>";

print "<p>Email: $email </p>";

print "<p>City: $city </p>";

print "<p>Section : $section</p>";


//***************************************
//Write to Patrons File
//***************************************

$filename = 'data/'.'patrons.txt';

$fp = fopen($filename, 'a');   //opens the file for appending

$contact_date = date('Y-m-d');

$output_line = $lastname.'|'.$firstname.'|'.$email.'|'.$city.'|'.$birth_yyyy.'|'."\n";

fwrite($fp, $output_line);

fclose($fp );

?>

<p>For Admin Use Only: <a href="assignment_3_view_patrons_new.php">View Patrons</a></p>

</div>
</body>
</html>