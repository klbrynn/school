<!DOCTYPE HTML>

<html>
<head>
	<title>Assignment 3 - View Patrons</title>
	<link rel="stylesheet" type="text/css" href="css/kinglib_3.css" />
</head>

<body>

<div id="logo">
	<img src="images/KingLibLogo.jpg" alt="King Real Estate Logo">
</div>

<div id="patronlist">
<h1>View Patrons</h1>

<table border='1'>
<tr>
	<th>Last Name</th>
	<th>First Name</th>
	<th>Email</th>
	<th>City</th>
	<th>Birth Year</th>
</tr>

<?php

//*****************************************************
//Read Guestbook Information From File Into HTML table
//*****************************************************

$display = "";
$line_ctr = 0;


$filename = 'data/'.'patrons.txt';

if (file_exists($filename))
{

	$fp = fopen($filename, 'r');   //opens the file for reading

	while(true)
	{
		$line = fgets($fp);

		if (feof($fp))
		{
			break;
		}

		$line_ctr++;

		$line_ctr_remainder = $line_ctr % 2;

		if ($line_ctr_remainder == 0)
		{
			$style = "style='background-color: #FFFFCC;'";
		} else {
			$style = "style='background-color: white;'";
		}

		list($lastname, $firstname, $email, $city, $birth_yyyy) = explode('|', $line);

		$display .= "<tr $style>";
			$display .= "<td>".$lastname."</td>";
			$display .= "<td>".$firstname."</td>";
			$display .= "<td>".$email."</td>";
			$display .= "<td>".$city."</td>";
			$display .= "<td>".$birth_yyyy."</td>";
		$display .= "</tr>\n";  //added newline
	}


	fclose($fp );

	print $display;

}
else
{
	print "<p>No Patrons Found</p>";
}

?>

</table>

</div>
</body>
</html>