<html>
    <head>
     <link rel="stylesheet" type="text/css" href="css/kinglib_6.css">
</head>
        <body>
             <img src="http://profperry.com/Classes20/PHPwithMySQL/KingLibLogo.jpg" > 
<div id="patron">
<?php

    include "assignment_6_db_functions.php";
    $db = connectDatabase();
    
//******************************
// Gather data
//******************************

    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $birthyear = $_POST['birthyear'];
    $city = $_POST['city'];
    
    $current_year = date('Y');
    $age = $current_year - $birthyear;

//******************************
// Add data to table
//******************************      

if (isset($_POST['submit']))
{
    $submitButton = trim($_POST['submit']);
} else {
    $submitButton = '';
}

if ($submitButton == 'Submit')
{
        if (isset($_POST['firstname']))
        {
            $firstname = trim($_POST['firstname']);
        } else {
            $firstname = '';
        }
    
            if (isset($_POST['lastname']))
        {
            $lastname = trim($_POST['lastname']);
        } else {
            $lastname = '';
        }
    
            if (isset($_POST['email']))
        {
            $email = trim($_POST['email']);
        } else {
            $email = '';
        }
    
            if (isset($_POST['birthyear']))
        {
            $birthyear = trim($_POST['birthyear']);
        } else {
            $birthyear = '';
        }
    
            if (isset($_POST['city']))
        {
            $city = trim($_POST['city']);
        } else {
            $city = '';
        }
} 
    
//******************************
// Validate data
//******************************  
    
if (empty($firstname) || empty($lastname) || empty($email) || empty($birthyear) || empty($city))
{
    print "<p style='color: red'> All fields are required </p>";
} else {
    $rtninfo = insertPatron($db, $firstname, $lastname, $email, $city, $birthyear);
    
    if ($rtninfo == "NotAdded")
    {
            print "<p style='color: red'> Patron not added </p>";
    } else {
            print "<p style='color: green'> Patron $firstname $lastname has been added"; 
    }
}

 /*           
$error_count = 0;
if (empty($firstname)) 
    {
        print "<p>Error: You must enter a first name </p>";
        $error_count++;
        print "</body></html>";
    }
            
if (empty($lastname)) 
    {
        print "<p>Error: You must enter a last name </p>";
        $error_count++;
        print "</body></html>";
    }
if (empty($email)) 
    {
        print "<p>Error: You must enter your email </p>";
        $error_count++;
        print "</body></html>";
    }
if (empty($birthyear)) 
    {
        print "<p>Error: You must enter your birth year </p>";
        $error_count++;
    } else { 
        if (!is_numeric($birthyear))
        {
            print "Error: Birth year must be numeric";
        }

    }
if (empty($city)) 
    {
        print "<p>Error: You must select a city</p>";
        $error_count++;
        print "</body></html>";
    }
if ($city == '-')
{
    print "<p>Error: You must enter a city</p>";
        $error_count++;
}
            
if ($error_count > 0)
    {
        print "Go back and make corrections";
        exit;
    }
*/
//******************************
// Calculate age
//******************************  

            
if ($age > 55)
    {
        $section = "Senior"; 
    } elseif ($age > 16 ) 
    {   
        $section = "Adult";
    } else {
        $section = "Children";
    }
    
//******************************
// Display info
//******************************  
           
$fullname = "$firstname $lastname";
print  "<p> <b> Thank you for registering!</b></p>";
//print  "<div class='divinfo>"; using this breaks the returned data
print   "<p> Name: $fullname</p>";
print   "<p>Email: $email</p>";
print   "<p>City: $city </p>";
print   "<p>Section: $section </p>";   
//print   "</div>";
    

        
print "<p>For Admin Use Only: <a href='assignment_6_view_patrons.php'>View Patrons</a> </p>";         


    
?>
    
    
            
   </div> 
</body>
</html>