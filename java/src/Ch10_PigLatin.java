import java.util.Scanner;

/*****************************************
 File: Ch10_PigLatin.java
 Author: Kelsey Lisenbe
 Assignment: Week 10 - OOP

 Requirements:
 Each word to lowercase before translating
 If the word starts with a vowel, just add way to the end of the word.
 If the word starts with a consonant, move all of the consonants that appear before the first vowel to the end of the word, then add ay to the end of the word.
 If a word starts with the letter y, the y should be treated as a consonant. If the y appears anywhere else in the word, it should be treated as a vowel.
 Check that the user has entered text before performing the translation.
 ****************************************/


public class Ch10_PigLatin {

    public static void main(String[] args) {

        System.out.println("Welcome to the Pig Latin Translator");
        System.out.println();

        String choice = "y";
        Scanner sc = new Scanner(System.in);

        while (choice.equalsIgnoreCase("y")) {
            System.out.println("Enter a line to be translated to Pig Latin:");
            //check for input before translating - THIS DOESNT WORK! HA
//            if (sc.nextLine().isEmpty()) {
//                sc.close();
//            }

            //lower case the input
            String input = sc.nextLine();
            String lcInput = input.toLowerCase();

            //translate the input here
            try {
                String[] words = lcInput.split(" ");

                //loop through the array and print the word/string?
                for (String word : words) {
                    if (word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") || word.startsWith("u")
                    )
                        System.out.print(word.toLowerCase() + "way ");
                    else if(word.startsWith("y")){
                        System.out.println(word.substring(1).toLowerCase() + word.substring(0, 1).toLowerCase() + "ay ");
                    }

                    else {
                        System.out.print(word.substring(1).toLowerCase() + word.substring(0, 1).toLowerCase() + "ay ");
                    }
                }

            } catch (Exception e) {
                System.out.println("Please enter a string to translate");
            }


            //continue translating?
            System.out.println();
            System.out.println("Continue y/n: ");
            choice = sc.nextLine();
        }
        sc.close();

    }
}
