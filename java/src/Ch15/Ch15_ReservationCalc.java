package Ch15;

import java.text.DateFormat;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.Scanner;
import java.time.LocalDate;


/*****************************************
 File: Ch15.Ch15_ReservationCalc.java
 Author: Kelsey Lisenbe
 Assignment: Week 12 - OOP
 ****************************************/

public class Ch15_ReservationCalc {

    Ch15_Reservation reservation = new Ch15_Reservation();
    public static void main(String[] args) {
        Ch15_Reservation reservation = new Ch15_Reservation();
        System.out.println("Welcome to the Reservation Calculator");
        System.out.println();


        String choice = "y";
        Scanner sc = new Scanner(System.in);

        while (choice.equalsIgnoreCase("y")) {
            System.out.println("Enter the arrival month (1-12):");
            int arrivalMonth = Integer.parseInt(sc.nextLine());
            System.out.println("Enter the arrival day (1-31):");
            int arrivalDay = Integer.parseInt(sc.nextLine());
            System.out.println("Enter the arrival year:");
            int arrivalYear = Integer.parseInt(sc.nextLine());

            int yA = Integer.parseInt(String.valueOf(arrivalYear));
            int mA = Integer.parseInt(String.valueOf(arrivalMonth));
            int dA = Integer.parseInt(String.valueOf(arrivalDay));
            LocalDate arrivalDate = LocalDate.of( yA , mA , dA );

            System.out.println("Enter the departure month (1-12):");
            int departureMonth = sc.nextInt();
            System.out.println("Enter the departure day (1-31):");
            int departureDay = sc.nextInt();
            System.out.println("Enter the departure year:");
            int departureYEar = sc.nextInt();

            DateTimeFormatter dtf = DateTimeFormatter.ofLocalizedDate(
                    FormatStyle.LONG);
            String formattedDate = dtf.format(arrivalDate);

            int yD = Integer.parseInt(String.valueOf(departureYEar));
            int mD = Integer.parseInt(String.valueOf(departureMonth));
            int dD = Integer.parseInt(String.valueOf(departureDay));
            LocalDate departureDate = LocalDate.of( yD , mD , dD );

            DateTimeFormatter dtf2 = DateTimeFormatter.ofLocalizedDate(
                    FormatStyle.LONG);
            String formattedDate2 = (String) dtf.format(departureDate);

            int compare = departureDate.compareTo(arrivalDate);

            //Return results
            System.out.println("Arrival Date: " + formattedDate);
            System.out.println("Departure Date: " + formattedDate2);
            System.out.print("Price Per Night " + reservation.getPricePerNightFormatted() + "\n");
            System.out.println("Total price: for " + compare + " nights");

            System.out.println("Continue? (y/n)");
            choice = sc.nextLine();
        }

        sc.close();
    }

}