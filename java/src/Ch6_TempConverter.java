import java.util.Scanner;
import java.text.NumberFormat;

/*****************************************
 File: Ch6_TempConverter.java
 Author: Kelsey Lisenbe
 Assignment: Week 6 - OOP
 ****************************************/

public class Ch6_TempConverter {

    public static void main(String[] args) {
        String choice = "y";
        Scanner sc = new Scanner(System.in);

        System.out.println("Welcome to the Temperature Converter!");
        System.out.println();

        while (choice.equalsIgnoreCase("y")) {
            // get user input
            //System.out.print("Enter degrees in Fahrenheit: ");
            //do this instead via professor
            System.out.print("Enter degrees in Fahrenheit: ");
            double fahrenheit = Double.parseDouble(sc.nextLine());

            // get value and convert
//            Double f = Double.valueOf((sc.nextLine()));
            Double c = Double.valueOf((fahrenheit - 32) * 5 / 9);
            Double roundF = Math.round(c * 100.0) / 100.0;

            // format the Celcius output per professor.
            NumberFormat number = NumberFormat.getNumberInstance();
            number.setMaximumFractionDigits(2);
            String celciusString = number.format(c);

            System.out.println("Degrees in Celsius: " + celciusString);
            System.out.println();
            System.out.println("Continue? y/n: ");
            choice = sc.nextLine();
        }
        sc.close();
    }
}
