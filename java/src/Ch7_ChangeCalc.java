import java.util.Scanner;

/*****************************************
 File: Ch7_ChangeCalc.java
 Author: Kelsey Lisenbe
 Assignment: Week 7 - OOP
 ****************************************/

public class Ch7_ChangeCalc {
    //methods to calculate cents
    public static int getQuarters(int cents) {
        return (int) Math.floor(cents / 25.0);
    }

    public static int getDimes(int cents) {
        return (int) Math.floor(cents / 10.0);
    }

    public static int getNickles(int cents) {
        return (int) Math.floor(cents / 5.0);
    }

    public static int getPennies(int cents) {
        return (int) Math.floor(cents / 1.0);
    }

    public static void main(String[] args) {
        String choice = "y";
        Scanner sc = new Scanner(System.in);

        System.out.println("Welcome to the Change Calculator!");
        System.out.println();

        while (choice.equalsIgnoreCase("y")) {
            System.out.print("Enter number of cents (0-99): ");
            System.out.println();
            int cents = sc.nextInt();
            int quarter = 25;
            int dime = 10;
            int nickel = 5;
            int penny = 1;

            //order matters here.
            int totalQuarter = (cents / quarter);
            cents %= quarter;

            int totalDime = (cents / dime);
            cents %= dime;

            int totalNickel = (cents / nickel);
            cents %= nickel;

            int totalPenny = (cents / penny);
            //no need for cents%= penny;
            int quantity = 5;
            int total = quantity++;

            double x = Math.random() * 10 + 100;

            String message = "Quarters: " + totalQuarter + "\n" +
                    "Dimes: " + totalDime + "\n" +
                    "Nickles " + totalNickel + "\n" +
                    "Pennies: " + totalPenny + "\n";

            System.out.println(message);
            System.out.print("Continue? y/n");
            System.out.println("this is the answer~!" + x);
            choice = sc.next();
        }
        sc.close();
    }
}
