import java.util.Scanner;


/*****************************************
 File: CustomerDB.java
 Author: Kelsey Lisenbe
 Assignment: Week 4 - OOP
 ****************************************/

public class CustomerApp {

    public static void main (String[] args){
        System.out.println("Welcome to the Customer Viewer");
        System.out.println();

            Scanner sc = new Scanner(System.in);
            String choice = "y";
        while(choice.equalsIgnoreCase("y")) {
            System.out.print("Enter a customer number: ");
            int customerNumber = Integer.parseInt(sc.nextLine());

            //try to get the customer object from CustomerDB else return an error
                Customer cus = CustomerDB.getCustomer(customerNumber);
                if (cus != null){
                    String message =
                            "\n"  + customerNumber + "\n"  +
                                    cus.getName() + "\n" +
                                    cus.getAddress() + "\n" +
                                    cus.getCity() + " " + cus.getState() + " " + cus.getPostalCode();
                    System.out.println(message);
                }
                else {
                    System.out.println("There is no customer number " + customerNumber + " in our records.");
                }

            System.out.print("\nDisplay another customer? (y/n): ");
            choice = sc.nextLine();
            System.out.println();

        }
        sc.close();
    }
}
