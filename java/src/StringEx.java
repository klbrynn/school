import java.util.Arrays;

public class StringEx {
    public static void main(String args[]) {
        double[] kilos = {200, 100, 48, 59, 72};
        for (int i = 0; i < kilos.length; i++) {

            System.out.println(kilos[i] *= 2.2);
        }

//        int[] values = {2, 1, 6, 5, 3};
//        Arrays.sort(values);
//        int index = Arrays.binarySearch(values, 5);
//        System.out.println(index);

//        String[][] names = new String[200][10];
//        int lists = names.length;
//        System.out.println(lists);

//        String s1 = "abc def ghi";
//        String s2 = s1.substring(1, 5);
//        String s3 = s2.replace('b', 'z');
//
//        System.out.println(s3);
    }

//    public static void test() {
//        String s1 = "805 Martin Street";
//        String s2 = "805 Martin Street";
//        if (s1.equals(s2)) {
//            System.out.println("same");
//        }
//        if (s1 == s2) {
//            System.out.println("equal");
//        }
//    }
//
//    public static void test2(){
//        String s1 = "Kristine";
//        s1 += " Thomas";
//        String s2 = s1;
//        if (s1.equals(s2)) {
//            System.out.println("same");
//        }
//        if (s1 == s2) {
//            System.out.println("equal");
//        }
//    }
//
//    public static void test3(){
//        StringBuilder s1 = new StringBuilder();
//        s1.append("115-88-9525");
//        s1.deleteCharAt(4);
//        System.out.println(s1);
//    }
//
//    public static void test4(){
//    double[][] times = { {23.0, 3.5}, {22.4, 3.6}, {21.3, 3.7} };
//    System.out.println(times[2][1]);
//    }
//
//    public static void test5() {
//        String[][] names = new String[200][10];
//        int lists = names.length;
//        System.out.println(lists);
//    }
}
