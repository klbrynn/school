import java.util.Scanner;

public class ch02_GradeConverter {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String repeatText = "Letter grade: ";
        String choice = "y";

        System.out.println("Welcome to the Letter Grade Converter");
        while (choice.equalsIgnoreCase("y")) {

            System.out.print("\nEnter numerical grade: ");
            String numericalGrade = sc.nextLine();
            int grade = Integer.parseInt(numericalGrade);

            //Set grade to range of values.
            if (grade >= 90 && grade <= 100) {
                System.out.println(repeatText + "A");
            } else if (grade >= 80 && grade <= 89) {
                System.out.println(repeatText + "B");
            } else if (grade >= 70 && grade <= 79) {
                System.out.println(repeatText + "C");
            } else if (grade >= 60 && grade <= 69) {
                System.out.println(repeatText + "D");
            } else if (grade <= 59) {
                System.out.println(repeatText + "F");
            }

            //Decide if the user wants to continue
            System.out.print("\nContinue? (y/n) ");
            choice = sc.nextLine();
        }
        //Return comment after exiting loop
        System.out.println("\nAssignment 1 is completed.");
        sc.close();
    }
}