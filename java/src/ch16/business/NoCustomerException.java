package ch16.business;

public class NoCustomerException extends Throwable {
    public NoCustomerException() {
    }

    public NoCustomerException(String message) {
        super(message);

    }
}
