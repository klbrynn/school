
/*****************************************
 File: Ch15.Ch16_CustomerViewer.java
 Author: Kelsey Lisenbe
 Assignment: Week 16 - OOP
 ****************************************/


package ch16.business;

import ch16.db.CustomerDB;
import java.util.Scanner;

public class ch16_CustomerViewer {

    public static void main(String args[]) throws NoCustomerException {
        System.out.println("Welcome To The Customer Viewer");
        getCusNum();
    }


    public static void getCusNum() throws NoCustomerException {
        Scanner sc = new Scanner(System.in);
        String choice = "y";

        while (choice.equalsIgnoreCase("y")) {
            System.out.println("\n" +"Enter Customer Number: ");
            int customerNumber = Integer.parseInt(sc.nextLine());
            Customer cus = CustomerDB.getCustomer(customerNumber);

            try {
                if (cus == null) {
                    throw new NoCustomerException(){
                    };
                } else {
                    System.out.println(cus.getNameAndAddress());
                }
            } catch (NoCustomerException e) {
                System.out.println("There is no customer with a number of " + customerNumber);
            }


            System.out.println("\n" +"Display another customer? (y/n): ");
            choice = sc.nextLine();

        }
    }
}

