package ch16.db;

import ch16.business.Customer;

public class CustomerDB {

    public static Customer getCustomer(int number) {
        Customer c = new Customer();
        if (number == 1000) {
            c.setName("Andrew Antosca");
            c.setAddress("485 Duane Terrace");
            c.setCity("Ann Arbor");
            c.setState("MI");
            c.setPostalCode("48108");
            return c;
        } else if (number == 1001) {
            c.setName("Barbara White");
            c.setAddress("3400 Richmond Parkway #3423");
            c.setCity("Bristol");
            c.setState("CT");
            c.setPostalCode("06010");
            return c;
        } else if (number == 1002) {
            c.setName("Karl Vang");
            c.setAddress("327 Franklin Street");
            c.setCity("Edina");
            c.setState("MN");
            c.setPostalCode("55435");
            return c;
        } else if (number == 1003) {
            c.setName("Ronda Chavan");
            c.setAddress("518 Comanche Dr.");
            c.setCity("Greensboro");
            c.setState("NC");
            c.setPostalCode("27410");
            return c;
        } else if (number == 1004) {
            c.setName("Sam Carol");
            c.setAddress("9379 N. Street");
            c.setCity("Long Beach");
            c.setState("CA");
            c.setPostalCode("90806");
            return c;
        } else {
            return null;
        }
    }
}