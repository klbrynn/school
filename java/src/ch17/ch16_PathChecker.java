package ch17;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

/*****************************************
 File: Ch17_PathChecker.java
 Author: Kelsey Lisenbe
 Assignment: Week 17 - OOP
 ****************************************/


public class ch16_PathChecker {


    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome to the Path Checker");

        String choice = "y";
        while (choice.equalsIgnoreCase("y")) {
            System.out.println("\n" + "Enter a path: ");
            String input = sc.nextLine();

            if (Files.exists(Paths.get(input))) {
                try (
                        BufferedReader in = new BufferedReader(
                                new FileReader(input))) {
                    String line = in.readLine();

                    while (line != null) {

                        System.out.print("\n" + "That path points to a regular file. " + "\n"
                                + "\n" + "First line of the file is: "
                                + "\n" + line);
                        line = in.readLine();
                    }
                } catch (IOException e) {
                    System.out.print("That path points to a directory.");
                }
            } else {
                System.out.println("That path does not exist. ");
            }

            System.out.println("\n" + "Continue? (y/n): ");
            choice = sc.nextLine();

        }
        sc.close();
    }
}

